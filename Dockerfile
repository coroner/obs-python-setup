FROM debian:11

MAINTAINER Szilárd Pfeiffer "coroner@pfeifferszilard.hu"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y --no-install-recommends fakeroot build-essential:native apt-file debhelper git-core curl \
 && apt-get install -y --no-install-recommends python3-pip python3-setuptools python3-wheel dh-python

RUN apt-get install -y --no-install-recommends gpg-agent gnupg2 \
 && curl -s 'http://download.opensuse.org/repositories/openSUSE:/Tools/Debian_11/Release.key' | apt-key add - \
 && echo 'deb http://download.opensuse.org/repositories/openSUSE:/Tools/Debian_11/ /' >/etc/apt/sources.list.d/obs.list \
 && apt-get update \
 && apt-get install -y --no-install-recommends osc

RUN pip install tomli

RUN apt-get autoremove --purge -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /var/tmp/* \
 && rm -rf /tmp/*

ADD templates /root/.config
ADD scripts/* /usr/local/bin/
ADD rpm /root/rpm
ADD deb /root/deb
