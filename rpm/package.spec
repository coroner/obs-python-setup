%define project_name $PROJECT_TECHNICAL_NAME
%define project_version $PROJECT_VERSION
%define project_unmangled_version $PROJECT_VERSION
%define project_release 1
%define project_description $PROJECT_DESCRIPTION
%define project_license $PROJECT_LICENSE
%define author_name $AUTHOR_NAME
%define author_email $AUTHOR_EMAIL

Summary: %{project_description}
Name: %{project_name}
Version: %{project_version}
Release: %{project_release}
Source0: %{project_name}_%{project_unmangled_version}.tar.gz
License: %{project_license}
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{project_name}-%{version}-%{project_release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: %{author_name} %{author_email}
BuildRequires: python3-setuptools

%description
%{project_description}

%prep
%setup -n %{project_name}

%build
python3 setup.py build

%install
python3 setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
