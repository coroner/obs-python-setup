#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import email.utils
import os
import sys
import tomli as tomllib

pyproject_file_path = sys.argv[1]

with open(pyproject_file_path, 'rb') as pyproject_file:
    pyproject = tomllib.load(pyproject_file)

    env_vars = {
        'PROJECT_NAME': pyproject['project']['name'],
        'PROJECT_VERSION': pyproject['project']['version'],
        'PROJECT_TECHNICAL_NAME': pyproject['tool']['variables']['technical_name'],
        'PROJECT_RELEASE_DATE': email.utils.formatdate(localtime=True),
        'PROJECT_LICENSE': pyproject['project']['license']['text'],
        'PROJECT_DESCRIPTION': pyproject['project']['description'],
        'AUTHOR_NAME': pyproject['project']['authors'][0]['name'],
        'AUTHOR_EMAIL': pyproject['project']['authors'][0]['email'],
        'RPM_BUILD_ROOT': '\$RPM_BUILD_ROOT',
    }

    print(os.linesep.join(map(lambda env_name: f'export {env_name}="{env_vars[env_name]}"', env_vars)))
