#!/usr/bin/env bash

set -ex

ENV_FILE=$(mktemp /tmp/env.XXXXXX)
env_vars.py ${CI_PROJECT_DIR}/pyproject.toml >${ENV_FILE}

source ${ENV_FILE}

find /root/.config/ -type f | xargs sed -i 's/.*/echo & | envsubst/e'

find /root/deb/ -type d | sed 's%^/root/deb/%%' | xargs -I{} mkdir -p ${CI_PROJECT_DIR}/debian/{}
find /root/deb/ -type f | sed 's%^/root/deb/%%' | xargs -I{} bash -c "envsubst </root/deb/{} >${CI_PROJECT_DIR}/debian/{}"

dpkg-buildpackage -S

export DEBIAN_FRONTEND=noninteractive
export REQUIREMENTS_DIR=${CI_PROJECT_DIR}

if [ -z "$CI_COMMIT_TAG" ]; then
        OBS_VERSION=dev
else
        OBS_VERSION=`echo $CI_COMMIT_TAG | cut -c 2- | sed 's%\.%:%g'`
fi

OBS_PROJECT="home:${OBS_USERNAME}:${OBS_PROJECT_NAME}:${OBS_VERSION}"
OBS_PROJECT_APT=`echo ${OBS_PROJECT} | sed "s#:#:/#g"`
OBS_PROJECT_APT_URL="https://download.opensuse.org/repositories/${OBS_PROJECT_APT}/Debian_11"

STATUSCODE=$(curl --silent --output /root/Release.key --write-out "%{http_code}" "${OBS_PROJECT_APT_URL}/Release.key")
if test $STATUSCODE -eq 200; then
        apt-key add /root/Release.key
        echo "deb ${OBS_PROJECT_APT_URL} /" >/etc/apt/sources.list.d/project.list
        apt-get update
fi

apt-file update
apt-file update /etc/apt/sources.list.d/project.list

export OBS_PROJECT_DIR=home:${OBS_USERNAME}:${OBS_PROJECT_NAME}:${OBS_VERSION}/${CI_PROJECT_NAME}

mkdir -p ${CI_PROJECT_DIR}/${OBS_PROJECT_DIR}
osc co -o ${CI_PROJECT_DIR}/${OBS_PROJECT_DIR} ${OBS_PROJECT_DIR}

rm -rf ${OBS_PROJECT_DIR}/*

mv ${CI_PROJECT_DIR}/../${PROJECT_TECHNICAL_NAME}_${PROJECT_VERSION}.dsc ${CI_PROJECT_DIR}/${OBS_PROJECT_DIR}/
mv ${CI_PROJECT_DIR}/../${PROJECT_TECHNICAL_NAME}_${PROJECT_VERSION}.tar.gz ${CI_PROJECT_DIR}/${OBS_PROJECT_DIR}/

envsubst </root/rpm/package.spec >${CI_PROJECT_DIR}/${OBS_PROJECT_DIR}/${PROJECT_TECHNICAL_NAME}.spec

osc addremove ${CI_PROJECT_DIR}/${OBS_PROJECT_DIR}
osc commit -m "Commited by GitLab (commit ${CI_COMMIT_SHORT_SHA})" ${CI_PROJECT_DIR}/${OBS_PROJECT_DIR}
